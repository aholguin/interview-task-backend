<?php

declare(strict_types=1);

namespace App\Modules\Invoices\Infrastructure\Providers;


use App\Modules\Invoices\Domain\InvoiceRepositoryInterface;
use App\Modules\Invoices\Infrastructure\Persistence\EloquentInvoiceRepository;
use Illuminate\Contracts\Support\DeferrableProvider;
use Illuminate\Support\ServiceProvider;

final class InvoiceServiceProvider extends ServiceProvider implements DeferrableProvider
{
    public function register(): void
    {
        $this->app->scoped(InvoiceRepositoryInterface::class, EloquentInvoiceRepository::class);
    }

    /**
     * @return array<class-string>
     */
    public function provides(): array
    {
        return [
            InvoiceRepositoryInterface::class,
        ];
    }
}
