<?php

declare(strict_types=1);

namespace App\Modules\Invoices\Infrastructure\Persistence;

use App\Domain\Enums\StatusEnum;
use App\Modules\Invoices\Domain\Entities\Invoice;
use App\Modules\Invoices\Domain\InvoiceRepositoryInterface;
use DateTimeImmutable;
use Exception;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Ramsey\Uuid\Uuid;

final class EloquentInvoiceRepository implements InvoiceRepositoryInterface
{
    /**
     * @throws Exception
     */
    public function search($uuid): ?Invoice
    {
        $model = $this->findByUuid($uuid);
        if (null === $model) {
            return null;
        }

        return new Invoice(
            Uuid::fromString($model->id),
            Uuid::fromString($model->number),
            new DateTimeImmutable($model->date),
            new DateTimeImmutable($model->due_date),
            $this->findCompanyData($model->company_id),
            $this->findProducts($model->id),
            StatusEnum::tryFrom($model->status),
        );
    }


    public function approve($uuid): void
    {
        DB::table('invoices')
            ->where('id', $uuid->toString())
            ->update(['status' => StatusEnum::APPROVED]);
    }

    public function reprove($uuid): void
    {
        DB::table('invoices')
            ->where('id', $uuid->toString())
            ->update(['status' => StatusEnum::REJECTED]);
    }

    protected function findByUuid($uuid)
    {
        return DB::table('invoices')->find(
            $uuid,
            [
                'id',
                'number',
                'date',
                'status',
                'due_date',
                'company_id',
            ]
        );
    }

    private function findCompanyData($companyId): object
    {
        return DB::table('companies')->find($companyId);
    }

    private function findProducts($invoiceId): Collection
    {
        return DB::table('invoice_product_lines')
            ->select('*')
            ->join('products', 'product_id', '=', 'products.id')
            ->where('invoice_id', $invoiceId)->get();
    }
}
