<?php

declare(strict_types=1);

namespace App\Modules\Invoices\Domain;

use App\Modules\Invoices\Domain\Entities\Invoice;

interface InvoiceRepositoryInterface
{
    public function search($uuid): ?Invoice;

    public function approve($uuid): void;

    public function reprove($uuid): void;
}
