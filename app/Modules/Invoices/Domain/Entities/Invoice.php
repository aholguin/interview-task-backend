<?php

declare(strict_types=1);

namespace App\Modules\Invoices\Domain\Entities;

use App\Domain\Enums\StatusEnum;
use DateTimeInterface;
use Ramsey\Uuid\UuidInterface;

readonly class Invoice
{
    public function __construct(
        private UuidInterface $id,
        private UuidInterface $number,
        private DateTimeInterface $date,
        private DateTimeInterface $dueDate,
        private object $company,
        private object $products,
        private StatusEnum $status,
    ) {
    }

    public function getId(): UuidInterface
    {
        return $this->id;
    }

    public function getNumber(): UuidInterface
    {
        return $this->number;
    }

    public function getDate(): DateTimeInterface
    {
        return $this->date;
    }

    public function getDueDate(): DateTimeInterface
    {
        return $this->dueDate;
    }

    public function getCompany(): object
    {
        return $this->company;
    }

    public function getProducts(): object
    {
        return $this->products;
    }

    public function getStatus(): object
    {
        return $this->status;
    }
}
