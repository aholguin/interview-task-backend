<?php

declare(strict_types=1);

namespace App\Modules\Invoices\Application\Exceptions;

class NotFoundException extends \Exception
{
}
