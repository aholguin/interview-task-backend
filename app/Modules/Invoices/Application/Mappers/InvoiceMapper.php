<?php

declare(strict_types=1);

namespace App\Modules\Invoices\Application\Mappers;


final class InvoiceMapper
{
    public static function toArray($invoice, $total): array
    {
        return [
            'Invoice number' => $invoice->getNumber(),
            'Invoice date' => $invoice->getDate()->format('d/m/Y'),
            'Due date' => $invoice->getDueDate()->format('d/m/Y'),
            'Company' => self::mapCompany($invoice->getCompany()),
            'Billed company' => self::mapBillCompany($invoice->getCompany()),
            'Products' => self::mapProducts($invoice->getProducts()),
            'Total price' => number_format($total),
        ];
    }

    public static function mapCompany(object $company): array
    {
        return [
            'Name' => $company->name,
            'Street Address' => $company->street,
            'City' => $company->city,
            'Zip code' => $company->zip,
            'Phone' => $company->phone,
        ];
    }

    public static function mapBillCompany(object $company): array
    {
        return array_merge(
            self::mapCompany($company),
            ['Email address' => $company->email]
        );
    }

    public static function mapProducts(object $products): array
    {
        $productsMapped = [];
        foreach ($products as $product) {
            $productsMapped[] = [
                'Name' => $product->name,
                'Quantity' => $product->quantity,
                'Unit Price' => $product->price,
                'Total' => number_format($product->total),
            ];
        }
        return $productsMapped;
    }
}
