<?php

declare(strict_types=1);

namespace App\Modules\Invoices\Application\Services;

use App\Modules\Approval\Api\ApprovalFacadeInterface;
use App\Modules\Approval\Api\Dto\ApprovalDto;
use App\Modules\Invoices\Application\Exceptions\NotFoundException;
use App\Modules\Invoices\Application\Mappers\InvoiceMapper;
use App\Modules\Invoices\Domain\InvoiceRepositoryInterface;
use Ramsey\Uuid\UuidInterface;


class InvoiceService
{
    private int $total = 0;

    public function __construct(
        protected InvoiceRepositoryInterface $invoiceRepository,
        protected ApprovalFacadeInterface $approvalFacade,
    ) {
    }

    /**
     * @throws NotFoundException
     */
    public function getData(UuidInterface $uuid): array
    {
        $invoice = $this->invoiceRepository->search($uuid);

        if ($invoice === null) {
            throw new NotFoundException('Invoice do not exist');
        }

        $this->CalculateTotals($invoice->getProducts());

        return InvoiceMapper::toArray($invoice, $this->total);
    }

    /**
     * @throws NotFoundException
     */
    public function approveInvoice(UuidInterface $uuid): void
    {
        $invoice = $this->invoiceRepository->search($uuid);

        if ($invoice === null) {
            throw new NotFoundException('Invoice do not exist');
        }

        $this->approvalFacade->approve(
            new ApprovalDto(
                $uuid,
                $invoice->getStatus(),
                get_class($invoice),
            )
        );

        $this->invoiceRepository->approve($uuid);
    }

    /**
     * @throws NotFoundException
     */
    public function rejectInvoice(UuidInterface $uuid): void
    {
        $invoice = $this->invoiceRepository->search($uuid);

        if ($invoice === null) {
            throw new NotFoundException('Invoice do not exist');
        }

        $this->approvalFacade->approve(
            new ApprovalDto(
                $uuid,
                $invoice->getStatus(),
                get_class($invoice),
            )
        );

        $this->invoiceRepository->reprove($uuid);
    }

    private function CalculateTotals(object $products): void
    {
        foreach ($products as $product) {
            $product->total = $product->quantity * $product->price;
            $this->total = $this->total + $product->total;
        }
    }
}
