<?php

declare(strict_types=1);

namespace App\Modules\Invoices\Api\Http;

use App\Infrastructure\Controller;
use App\Modules\Invoices\Application\Services\InvoiceService;
use Exception;
use Illuminate\Http\JsonResponse;
use Ramsey\Uuid\Uuid;
use Symfony\Component\HttpFoundation\Response;

class InvoiceController extends Controller
{
    public function __construct(
        private readonly InvoiceService $invoiceService
    ) {
    }

    public function show(string $uuid): JsonResponse
    {
        try {
            $response = $this->invoiceService->getData(
                Uuid::fromString($uuid)
            );
        } catch (Exception $e) {
            return response()->json(['message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }

        return response()->json($response, Response::HTTP_OK);
    }

    public function approve(string $uuid): JsonResponse
    {
        try {
            $this->invoiceService->approveInvoice(
                Uuid::fromString($uuid)
            );
        } catch (Exception $e) {
            return response()->json(['message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }

        return response()->json(['message' => 'The invoice was approved'], Response::HTTP_OK);
    }

    public function reject(string $uuid): JsonResponse
    {
        try {
            $this->invoiceService->rejectInvoice(
                Uuid::fromString($uuid)
            );
        } catch (Exception $e) {
            return response()->json(['message' => $e->getMessage()], Response::HTTP_BAD_REQUEST);
        }

        return response()->json(['message' => 'Invoice rejected.'], Response::HTTP_OK);
    }
}
