<?php

declare(strict_types=1);

namespace Tests\Modules\Invoices\Application\Services;


use App\Domain\Enums\StatusEnum;
use App\Modules\Approval\Api\ApprovalFacadeInterface;
use App\Modules\Invoices\Application\Exceptions\NotFoundException;
use App\Modules\Invoices\Application\Services\InvoiceService;
use App\Modules\Invoices\Domain\Entities\Invoice;
use App\Modules\Invoices\Domain\InvoiceRepositoryInterface;
use Exception;
use Ramsey\Uuid\Uuid;
use Tests\CreatesApplication;

use PHPUnit\Framework\TestCase;

class InvoiceServiceTest extends TestCase
{

    use CreatesApplication;


    private InvoiceService $invoiceService;
    private InvoiceRepositoryInterface $invoiceRepositoryMock;
    private ApprovalFacadeInterface $approvalFacadeMock;

    protected function setUp(): void
    {
        parent::setUp();

        $this->createApplication();
        $this->invoiceRepositoryMock = $this->createMock(InvoiceRepositoryInterface::class);
        $this->approvalFacadeMock = $this->createMock(ApprovalFacadeInterface::class);
        $this->invoiceService = new InvoiceService(
            $this->invoiceRepositoryMock,
            $this->approvalFacadeMock
        );
    }

    /**
     * @test
     */
    public function itThrowsInvoiceNotFoundExceptionInGetDataMethod()
    {
        $uuid = Uuid::uuid1();
        $this->expectException(NotFoundException::class);
        $this->invoiceService->rejectInvoice($uuid);
    }

    /**
     * @test
     */
    public function itThrowsInvoiceNotFoundExceptionApproveInvoiceMethod()
    {
        $uuid = Uuid::uuid1();
        $this->expectException(NotFoundException::class);
        $this->invoiceService->approveInvoice($uuid);
    }

    /**
     * @test
     */
    public function itThrowsInvoiceNotFoundExceptionInRejectInvoiceMethod(): void
    {
        $uuid = Uuid::uuid1();
        $this->expectException(NotFoundException::class);
        $this->invoiceService->rejectInvoice($uuid);
    }

    /**
     * @test
     * @throws Exception
     */
    public function itThrowsLogicExceptionWhenTryToApproveAnInvoiceAlreadyApproved(): void
    {
        $uuid = Uuid::fromString('8bbf8a7d-7ac7-4157-a706-979c99b26b95');

        $invoice = new Invoice(
            Uuid::fromString('8bbf8a7d-7ac7-4157-a706-979c99b26b95'),
            Uuid::fromString('640d97d3-39c9-3fab-910e-492be086fe4f'),
            new \DateTimeImmutable('1981-12-26'),
            new \DateTimeImmutable('1988-01-29'),
            new \stdClass(),
            new \stdClass(),
            StatusEnum::tryFrom('approved'),
        );

        $this->invoiceRepositoryMock->method('search')->willReturn($invoice);
        $this->approvalFacadeMock->method('approve')->willThrowException(
            new \LogicException('approval status is already assigned')
        );
        $this->expectException(\LogicException::class);
        $this->invoiceService->approveInvoice($uuid);
    }
}
